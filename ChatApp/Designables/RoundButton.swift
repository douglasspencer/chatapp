//
//  RoundButton.swift
//  ChatApp
//
//  Created by Douglas Spencer on 7/19/17.
//  Copyright © 2017 Douglas Spencer. All rights reserved.
//

import UIKit

@IBDesignable
class RoundButton: UIButton {
    
    @IBInspectable var CornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = CornerRadius
            self.setNeedsLayout()
        }
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }
    
}
