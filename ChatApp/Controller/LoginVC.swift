//
//  LoginVC.swift
//  ChatApp
//
//  Created by Douglas Spencer on 7/18/17.
//  Copyright © 2017 Douglas Spencer. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {
    
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnLogin: RoundButton!
    @IBOutlet weak var btnSignUpLabelBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func btnClose_Pressed() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnSignUpLabel_Pressed(_ sender: Any) {
        
    }
    
    @IBAction func btnLogin_Pressed(_ sender: Any) {
        
    }
}
