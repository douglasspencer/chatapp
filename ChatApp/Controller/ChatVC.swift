//
//  ChatVC.swift
//  ChatApp
//
//  Created by Douglas Spencer on 7/18/17.
//  Copyright © 2017 Douglas Spencer. All rights reserved.
//

import UIKit

class ChatVC: UIViewController {
    
    @IBOutlet weak var btnHamburger: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        btnHamburger.addTarget(self.revealViewController, action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer()   )
    }
    
}
