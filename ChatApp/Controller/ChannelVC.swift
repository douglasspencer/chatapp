//
//  ChannelVC.swift
//  ChatApp
//
//  Created by Douglas Spencer on 7/18/17.
//  Copyright © 2017 Douglas Spencer. All rights reserved.
//

import UIKit

class ChannelVC: UIViewController {
    
    @IBOutlet weak var btnLogin: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.revealViewController().rearViewRevealWidth = self.view.frame.size.width - 60
    }
    
    
    @IBAction func btnLogin_Pressed() {
        performSegue(withIdentifier: SEGUE_LOGINVC, sender: nil)
    }
    
}
