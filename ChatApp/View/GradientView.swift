//
//  GradientView.swift
//  ChatApp
//
//  Created by Douglas Spencer on 7/18/17.
//  Copyright © 2017 Douglas Spencer. All rights reserved.
//

import UIKit

@IBDesignable
class GradientView: UIView {
    
    @IBInspectable var TopColor: UIColor = #colorLiteral(red: 0.3019607843, green: 0.568627451, blue: 0.8470588235, alpha: 1) {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable var BottomColor: UIColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1) {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let gradientLayer : CAGradientLayer = CAGradientLayer()
        gradientLayer.colors = [TopColor.cgColor,BottomColor.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x:1, y: 1)
        gradientLayer.frame = self.bounds
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
}
